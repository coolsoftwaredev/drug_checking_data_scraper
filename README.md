# Drug Checking Data Scraper

This project is currently set to be public, but we can restrict access if preferred, and we should be able to keep the output set to public even if the source is private.

The main script can be found at [drug_checking_data_scraper.py](/drug_checking_data_scraper.py) and has been copy/pasted from the previous setup. 

[requirements.txt](/requirements.txt) lists out the requirements the script needs to successfully run. Keeping in this repository ensures we can move the project around and run it in any environment that has pip, and can read [requirements files](https://pip.pypa.io/en/stable/reference/pip_install/?highlight=requirements#requirements-file-format).

## CI/CD

The [.gitlab-ci.yml](/.gitlab-ci.yml) file contains the configuration for the [CI/CD pipeline](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html).

The pipeline can be triggered in three ways: 

1. [Weekly on Mondays at 1am Pacific](https://gitlab.com/coolsoftwaredev/drug_checking_data_scraper/-/pipeline_schedules). [We can change the schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#pipeline-schedules).
1. Any time `drug_checking_data_scraper.py` or `gitlab-ci.yml` are changed and pushed to this repository. Specified with the [only keyword](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-advanced) 
1. [Manual triggers](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually)

And the pipeline goes through these steps:

1. Provision a docker container with the latest `python` image
1. Install and start a virtualenv in the docker
1. Install the requirements from `requirements.txt`
1. Execute `drug_checking_data_scraper.py`
1. Upload the `dcbc.csv` file to the artifacts for the job.

We get [400 free execution minutes per month](https://about.gitlab.com/pricing/), which should be sufficient for this script. 
## Artifacts

We use [artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#defining-artifacts-in-gitlab-ciyml) to make the CSV results available for download. 

Download the latest `dcbc.csv`: 

[https://gitlab.com/coolsoftwaredev/drug_checking_data_scraper/-/jobs/artifacts/master/raw/dcbc.csv?job=run](https://gitlab.com/coolsoftwaredev/drug_checking_data_scraper/-/jobs/artifacts/master/raw/dcbc.csv?job=run)

Browse the latest artifacts: 

[https://gitlab.com/coolsoftwaredev/drug_checking_data_scraper/-/jobs/artifacts/master/browse?job=run](https://gitlab.com/coolsoftwaredev/drug_checking_data_scraper/-/jobs/artifacts/master/browse?job=run)

You can also [browse artifacts for specific jobs](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#defining-artifacts-in-gitlab-ciyml). Artifacts [expire in 30 days by default](https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsexpire_in), but we can change that if we want them to stick around longer. 
